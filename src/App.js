import { useEffect, useState } from "react";
import "./App.css";
import Header from "./components/Header";
import products from "./product.json";
import Product from "./components/Product";
import Basket from "./components/Basket";

function App() {
  const [money, setMoney] = useState(100);
  const [basket, setBasket] = useState([]);
  const [total, setTotal] = useState(0);

  const resetBasket = () => {
    setBasket([]);
  };

  useEffect(() => {
    setTotal(
      basket.reduce((acc, item) => {
        return (
          acc +
          item.amount * products.find((product) => product.id === item.id).price
        );
      }, 0)
    );
  }, [basket]);

  return (
    <div className="App">
      <Header total={total} money={money} />
      {products.map((product) => (
        <Product
          key={product.id}
          basket={basket}
          setBasket={setBasket}
          product={product}
          total={total}
          money={money}
        />
      ))}
      <Basket products={products} basket={basket} />
      <button className="bg-red-200 px-3 rounded-lg" onClick={resetBasket}>
        Sepeti Sifirla
      </button>
    </div>
  );
}

export default App;
