import React from "react";

const Product = ({ product, basket, setBasket, total, money }) => {
  const basketItem = basket.find((item) => item.id === product.id);

  const addBasket = () => {
    const checkBasket = basket.find((item) => item.id === product.id);
    if (checkBasket) {
      checkBasket.amount += 1;
      setBasket([
        ...basket.filter((item) => item.id !== product.id),
        checkBasket,
      ]);
    } else {
      setBasket([
        ...basket,
        {
          id: product.id,
          amount: 1,
        },
      ]);
    }
  };

  const removeBasket = () => {
    const currentBasket = basket.find((item) => item.id === product.id);
    const basketWithoutCurrent = basket.filter(
      (item) => item.id !== product.id
    );
    currentBasket.amount -= 1;
    if (currentBasket.amount === 0) {
      setBasket([...basketWithoutCurrent]);
    } else {
      setBasket([...basketWithoutCurrent, currentBasket]);
    }
  };
  return (
    <div>
      <div className="product p-5 bg-white border mb-2">
        <h3>{product.title}</h3>
        <div>${product.price}</div>
        <div className="flex justify-center gap-3">
          <button
            className="bg-gray-200 px-3 rounded-lg"
            onClick={removeBasket}
            disabled={!basketItem}
          >
            Sat
          </button>
          <span>{(basketItem && basketItem.amount) || 0}</span>
          <button
            className="bg-gray-200 px-3 rounded-lg"
            onClick={addBasket}
            disabled={total + product.price > money}
          >
            Satin al
          </button>
        </div>
      </div>
    </div>
  );
};

export default Product;
