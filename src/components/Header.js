import React from "react";

const Header = ({ money, total }) => {
  return (
    <div>
      {total > 0 && money - total !== 0 && (
        <div>Harcayacak ${money - total} paraniz kaldi.</div>
      )}
      {total === 0 && <div>Harcamak icin ${money} paraniz var.</div>}
      {money - total === 0 && <div>Paraniz Bitti</div>}
    </div>
  );
};

export default Header;
